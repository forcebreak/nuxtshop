import * as firebase from 'firebase'
import firebaseConf from '../plugins/firebase'
import 'firebase/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  products: [],
  categories: [],
  singleProduct: null,
  singleCategory: null,
  productsInCart: []
})

export const getters = {
  allProducts (state) {
    return state.products
  },
  allCategories (state) {
    return state.categories
  },
  singlePRODUCT (state) {
    return state.singleProduct
  },
  singleCATEGORY (state) {
    return state.singleCategory
  },
  productsInCART (state) {
    return state.productsInCart
  }
}

export const mutations = {
  GET_PRODUCTS (state, payload) {
    let products = []
    payload.forEach(s => {
      const data = s.data()
      products.push(data)
    })
    state.products = products
  },
  GET_CATEGORIES (state, payload) {
    let categories = []
    payload.forEach(s => {
      const data = s.data()
      categories.push(data)
    })
    state.categories = categories
  },
  GET_SINGLE_PRODUCT (state, payload) {
    let product = payload.data()
    state.singleProduct = product
  },
  GET_SINGLE_CATEGORY (state, payload) {
    let category = payload.data()
    state.singleCategory = category
  },
  ADD_PRODUCT_TO_CART (state, payload) {
    state.productsInCart.push(payload)
    let serializePayload = JSON.stringify(payload)
    sessionStorage.setItem(payload.doc, serializePayload)
  },
  REMOVE_PRODUCT_FROM_CART (state, payload) {
    sessionStorage.removeItem(payload.id)
    state.productsInCart.splice(payload.index, 1)
  },
  CART_STATE (state, payload) {
    state.productsInCart = payload
  }
}
export const actions = { 
  async GET_PRODUCTS ({commit}, payload) {
    await db.collection('products').get()
    .then((doc) => {
      commit('GET_PRODUCTS', doc)
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки товаров', text: error, type: 'error' },{root: true})
    })
  },
  async GET_CATEGORIES ({commit}, payload) {
    await db.collection('category').get()
    .then((doc) => {
      commit('GET_CATEGORIES', doc)
    })
    .catch(error => {
      //commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  },
  async GET_SINGLE_PRODUCT ({commit}, payload) {
    await db.collection('products').doc(payload).get()
    .then((doc) => {
      commit('GET_SINGLE_PRODUCT', doc)
      commit('notify/NOTIFY', {title: 'Норм загрузки постов', text: error, type: 'success' },{root: true})
    })
    .catch(error => {
      //commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  },
  async GET_SINGLE_CATEGORY ({commit}, payload) {
    await db.collection('category').doc(payload).get()
    .then((doc) => {
      commit('GET_SINGLE_CATEGORY', doc)
      commit('notify/NOTIFY', {title: 'Категория загружена', text: error, type: 'success' },{root: true})
    })
    .catch(error => {
      //commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  }
}