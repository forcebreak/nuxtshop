import * as firebase from 'firebase'
import firebaseConf from '../plugins/firebase'
import 'firebase/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  sliders: [],
  slider:[],
  sliderName: ''
})

export const getters = {
  sliders (state) {
    return state.sliders
  },
  slider (state) {
    return state.slider
  },
  sliderName(state) {
    return state.sliderName
  }
}

export const mutations = {
  GET_SLIDERS(state, payload) {
    let sliders = []
    payload.forEach(s => {
      const data = s.data()
      sliders.push(data)
    })
    state.sliders = sliders
  },
  GET_SLIDER(state, payload) {
    let slider = []
    payload.forEach(s => {
      const data = s.data()
      slider.push(data)
    })
    state.slider = slider
  },
  GET_SLIDER_NAME(state, payload) {
    state.sliderName = payload
  },
  CHANGE_SLIDE(state, payload) {
    if(state.slider.length == payload.index){
      state.slider.push({
        img: '',
        title: '',
        index: ''
      })
    }
    state.slider[payload.index].img = payload.img
    state.slider[payload.index].title = payload.title
    state.slider[payload.index].index = payload.index
  },
  DELETE_SLIDE(state, payload) {
    let indexToDelete = null
    for (let i = 0; i < state.slider.length; i++) {
      if(state.slider[i].index == payload){
        indexToDelete = i
      }
    }
    state.slider.splice(indexToDelete, 1)
  },
  RECOUNT_SLIDERS(state, payload) {
    state.sliders.splice(payload.index, 1)
  }
}
export const actions = {
  async GET_SLIDERS({commit}, payload) {
    await db.collection('sliders').get()
      .then((doc) => {
        commit('GET_SLIDERS', doc)
      })
  },
  async GET_SLIDER({commit}, payload) {
    commit('GET_SLIDER_NAME', payload)
    await db.collection('sliders').doc(payload).collection('slider').get()
      .then((res) => {
        commit('GET_SLIDER', res)
      })
  },
  SET_SLIDER({commit}, payload) {
    commit('SET_PROCESSING', true, {root: true})
    for (let index = 0; index < payload.slider.length; index++) {
      let uploadTask = firebase.storage().ref().child('sliders').child(payload.name).child(payload.name + index).put(payload.slider[index].img);

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        function() {
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            db.collection('sliders').doc(payload.name).set({
              name: payload.name
            })
            db.collection('sliders').doc(payload.name).collection('slider').doc(`slide${index}`).set({
              img: downloadURL,
              title: payload.slider[index].title,
              index: payload.slider[index].index,
              link: payload.slider[index].link
            }, { merge: true })
          })
        })
      
      if(index < payload.slider.length) {
        setTimeout(function(){
          commit('SET_PROCESSING', false, {root: true})
        }, 1500)
      }
    }
  },
  CHANGE_SLIDE({commit}, payload){
    let uploadTask = firebase.storage().ref()
    .child('sliders').child(payload.name).child(payload.name + payload.index)
    .put(payload.slider.img);
    
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      function() {
        uploadTask.snapshot.ref.getDownloadURL()
        .then(
          function(downloadURL) { 
            db.collection('sliders').doc(payload.name).collection('slider').doc(`slide${payload.index}`).set({
              img: downloadURL,
              title: payload.slider.title,
              index: payload.slider.index
            }, { merge: true })
            commit('CHANGE_SLIDE', {img:downloadURL, title:payload.slider.title, index: payload.index})
          }
        )
      })
  },
  DELETE_SLIDE({commit}, payload) {
    db.collection("sliders").doc(payload.name).collection("slider").doc(`slide${payload.index}`)
    .delete()
    .then(function() {
      commit('DELETE_SLIDE', payload.index)

      firebase.storage().ref()
      .child('sliders').child(payload.name).child(payload.name + payload.index).delete()
      .then(function() {
        commit('notify/NOTIFY', {title: 'Слайд удален', type: 'success' },{root: true})
      }).catch(function(error) {
        commit('notify/NOTIFY', {title: 'Слайд не удален', text: error, type: 'error' },{root: true})
      });
    }).catch(function(error) {
        console.error("Error removing document: ", error);
    });
  },
  async DELETE_SLIDER({commit}, payload) {
    let countSlides = await db.collection('sliders').doc(payload.name).collection('slider').get()
    commit('DELETE_SLIDER', countSlides.docs.length)
    for (let i = 0; i < countSlides.docs.length; i++) {
      try {
        await firebase.storage().ref().child('sliders').child(payload.name).child(payload.name + i).delete()
        commit('notify/NOTIFY', {title: `Слайд ${payload.name + i}`, text: 'Успешно удален', type: 'success' },{root: true})
      } catch(err) {
        console.log(err)
        commit('notify/NOTIFY', {title: 'Слайд не удален', text: err, type: 'error' },{root: true})
      }
    }
    try{
      await db.collection("sliders").doc(payload.name).delete()
      commit('RECOUNT_SLIDERS', payload)
      commit('notify/NOTIFY', {title: `Слайдер ${payload.name}`, text: 'Успешно удален из БД', type: 'success' },{root: true})
    } catch(err) {
      commit('notify/NOTIFY', {title: 'Слайд не удален', text: err, type: 'error' },{root: true})
    }
  }
}