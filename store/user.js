import * as firebase from 'firebase'

export const state = () => ({
  showLogin: false,
  showRegister: false,
  user: {
    isAuthenticated: false,
    userId: null,
    userEmail: null,
    login: 'Аноним',
    emailVerified: false,
    isAdmin: false
  },
  avatarProgress: 0,
  avatarImg: null
})

export const getters = {
  avatarProgress (state) {
    return state.avatarProgress
  },
  avatarImg (state) {
    return state.avatarImg
  },
  emailVeryfied: (state) => {
    return state.user.emailVerified
  },
  isLogged (state) {
    if (state.user.isAuthenticated){
      return true
    }else{
      return false
    }
  },
  myLogin (state) {
    return state.user.login
  },
  myEmail (state) {
    return state.user.userEmail
  },
  myId (state) {
    return state.user.userId
  }
}

export const mutations = {
  SET_USER (state, payload) {
    state.user.isAuthenticated = true
    state.user.userId = payload.userId
    state.user.userEmail = payload.userEmail
    state.user.emailVerified = payload.emailVerified
    state.showLogin = false
    state.showRegister = false
    if(payload.avatar){
      state.avatarImg = payload.avatar
    }
    if (payload.login){
      state.user.login = payload.login
    }
    if (payload.userEmail == 'gbuecjd@ukr.net'){
      state.user.isAdmin = true
    }
    
  },
  UNSET_USER (state, payload) {
    state.user = {
      isAuthenticated: false,
      userId: null,
      userEmail: null,
      login: 'Аноним',
      emailVerified: false,
      avatarImg: null,
      isAdmin: false
    }
  },
  SHOW_REG (state, payload) {
    state.showRegister = payload
  },
  SHOW_SIGNIN (state, payload) {
    state.showLogin = payload
  },
  SETING_LOGIN (state, payload){
    state.user.login = payload
  },
  CHANGING_EMAIL (state, payload) {
    state.user.userEmail = payload
    state.user.emailVerified = false
  },
  VERIFING_EMAIL (state, payload) {
    state.user.emailVerified = payload.emailVerified
  },
  AVATAR_PROGRESS (state, payload) {
    let progress = (payload.bytesTransferred / payload.totalBytes) * 100;
    state.avatarProgress = progress
    switch (payload.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  },
  AVATAR_PROGRESS_RESET (state, payload) {
    state.avatarProgress = payload
  },
  SET_AVATAR (state, payload) {
    state.avatarImg = payload
  }
}
export const actions = {
  SIGNUP ({ commit }, payload) {
    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
      .then(user => {
        commit('SET_PROCESSING', true, {root: true})
        commit('SET_USER', 
        {
          userId: user.user.uid, 
          userEmail: user.user.email
        }),
        commit('SETING_LOGIN', 'Аноним'),
        commit('SET_PROCESSING', false, {root: true})
        commit('notify/NOTIFY', {
          title: `Привет ${payload.login}`, 
          text: 'Укажи своё имя на странице Мой кабинет', 
          type: 'success' 
        }, 
        {root: true})

        firebase.firestore().collection('users').doc(user.user.uid).set({
          userId: user.user.uid, 
          userEmail: user.user.email,
          emailVerified: false
        })
        .then(response => {
          commit('SET_COOKIE', {
            name: 'userId',
            value: user.user.uid
          }, {root: true})
        })
      })
      .catch(function (error) {
        commit('notify/NOTIFY',{
          title: 'Ошибка!', 
          text: error.message, 
          type: 'error'}, 
          {root: true})
      })
  },
  SIGNIN ({ commit }, payload) {
    firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
    .then(user =>{
      commit('SET_PROCESSING', true, {root: true})
      commit('SET_USER', {
        userId: user.user.uid, 
        userEmail: user.user.email, 
        emailVerified: user.user.emailVerified,
        avatar: payload.avatar
      })
      commit('isAdmin/IS_ADMIN', payload.email, {root: true})
      commit('SET_PROCESSING', false, {root: true})

      //Делаем запрос на информацию о юзере
      firebase.firestore().collection('users').doc(user.user.uid).get()
      .then(response => {
        //Устанавливаем куки по id юзера
        commit('SET_COOKIE', {
          name: 'userId',
          value: response.id
        }, {root: true})
        //Если в базе есть логин, устанавливаем его
        if (response.data().login){
          commit('SETING_LOGIN', response.data().login)
          commit('notify/NOTIFY', {
            title: 'Привет', 
            text: response.data().login, 
            type: 'success' 
          }, 
          {root: true})
        }else{
          commit('SETING_LOGIN', 'Аноним')
          commit('notify/NOTIFY', {
            title: 'Привет', 
            text: 'Аноним', 
            type: 'success' 
          }, 
          {root: true})
        }
      })

      //Делаем запрос для получения аватарки юзера
      firebase.storage().ref().child(`users/${user.user.uid}`).getDownloadURL().then(function(url) {
        commit('SET_AVATAR', url)
      }).catch(function(error) {
        switch (error.code) {
          case 'storage/object-not-found':
          commit('SET_AVATAR', null)
            break;
          case 'storage/unauthorized':
          commit('SET_AVATAR', null)
            break;
          case 'storage/canceled':
          commit('SET_AVATAR', null)
            break;
          case 'storage/unknown':
          commit('SET_AVATAR', null)
            break;
        }
      })
    })
    .catch(function(error) {
      commit('notify/NOTIFY', {title: 'Ошибка!', text: error.message, type: 'error' }, {root: true})
    })
  },
  SIGNOUT ({ commit }, payload) {
    firebase.auth().signOut()
    .then(function(){
      commit('SET_COOKIE', {
        name: 'userId',
        value: 'default'
      }, {root: true})
      commit('UNSET_USER', {root: true})
      commit('isAdmin/IS_ADMIN', payload.email, {root: true})
      commit('notify/NOTIFY', {title: 'Пока', text: payload.login, type: 'success' }, {root: true})
    })
  },
  EMAIL_CHECK ({ commit }, payload) {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user.emailVerified) {
        firebase.firestore().collection('users').doc(user.uid).update({
          emailVerified: user.emailVerified
        })

        firebase.firestore().collection('users').doc(user.uid).get()
        .then(response => {
          commit('VERIFING_EMAIL', response.data())
        })
      }else{
        commit('notify/NOTIFY', {title: 'Ошибка!', text: 'Email не подтверждён', type: 'error' }, {root: true})
      }
    })
  },
  VERIFY_EMAIL ({ commit }) {
    let user = firebase.auth().currentUser;
    user.sendEmailVerification()
    .then(function() {
      commit('notify/NOTIFY', {
        title: 'Отправлен запрос', 
        text: 'Подтвердите ваш email перейдя по ссылке в письме, потом совершите проверку нажав на кнопку', 
        type: 'success' 
      }, {root: true})
    }).catch(function(error) {
      commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'error' },{root: true})
    });
  },
  async SET_LOGIN ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    await firebase.firestore().collection('users').doc(payload.userId).set({
      login: payload.login
    }, { merge: true })

    await firebase.firestore().collection('users').doc(payload.userId).get()
    .then(response => {
      let login = response.data().login
      commit('SETING_LOGIN', login)
      commit('SETING_SET_AVATARLOGIN', null)
      commit('SET_PROCESSING', false, {root: true})
      commit('notify/NOTIFY', {title: 'Логин изменен!', text: `Ваш новый логин ${login}`, type: 'success' },{root: true})
    })
    .catch(function(error) {
      commit('notify/NOTIFY', {title: 'Логин не изменен!', text: error, type: 'error' },{root: true})
    });
  },
  CHANGE_EMAIL ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    let user = firebase.auth().currentUser;
    user.updateEmail(payload.email)
    .then(function() {
      commit('notify/NOTIFY', {
        title: 'Email изменен!', 
        text: `Ваш новый email ${payload.email}`, 
        type: 'success' 
      },
      {root: true})
    })
    .catch(function(error) {
      commit('notify/NOTIFY', {title: 'Почта не изменена!', text: error, type: 'error' },{root: true})
    })

    firebase.firestore().collection('users').doc(payload.userId).update({
      userEmail: payload.email,
      emailVerified: false
    })

    firebase.firestore().collection('users').doc(payload.userId).get()
    .then(response => {
      let userEmail = response.data().userEmail
      commit('CHANGING_EMAIL', userEmail)
      commit('SET_PROCESSING', false, {root: true})
    })

  },
  SET_AVATAR ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    // Upload file and metadata to the object 'images/mountains.jpg'
    let uploadTask = firebase.storage().ref().child('users/' + payload.id).put(payload.file);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function(snapshot) {
      commit('AVATAR_PROGRESS', snapshot)
    }, function(error) {

    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/unauthorized':
        // User doesn't have permission to access the object
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/canceled':
        // User canceled the upload
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/unknown':
        // Unknown error occurred, inspect error.serverResponse
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;
    }
    }, function() {
      // Upload completed successfully, now we can get the download URL
      commit('notify/NOTIFY', {title: 'Аватар загружен', type: 'success' },{root: true})
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        //Добавляем ссылку на аватарку в базу users
        firebase.firestore().collection('users').doc(payload.id).set({
          avatar: downloadURL
        }, { merge: true })

        commit('SET_AVATAR', downloadURL)
        commit('SET_PROCESSING', false, {root: true})

        setTimeout(() => {
          commit('AVATAR_PROGRESS_RESET', +0)
        }, 2000);
        
      })
    })
  }
}