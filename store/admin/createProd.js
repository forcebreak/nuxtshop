import * as firebase from 'firebase'
import firebaseConf from '../../plugins/firebase'
import 'firebase/firestore'
import { isContext } from 'vm';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  doc: '',
  img: '',
  imgProgress: 0,
  categories: []
})

export const getters = {
  isDoc (state) {
    return state.doc
  },
  isImg (state) {
    return state.img
  },
  prodCategories (state) {
    return state.categories
  }
}

export const mutations = {
  CREATE_PRODUCT (state, payload) {
    state.doc = payload
  },
  CREATE_PRODUCT_IMG (state, payload) {
    state.img = payload
  },
  GET_CATEGORIES (state, payload) {
    state.categories = payload
  },
  LOG (state, payload) {

  }
}
export const actions = { 
  CREATE_PRODUCT({ commit }, payload){
    db.collection('products').doc(payload.slug).set(payload)
    .then(() => {
      commit('CREATE_PRODUCT', payload.slug)
      commit('notify/NOTIFY', {title: 'Создана категория', text: payload.name, type: 'success' },{root: true})
    })
  },
  SET_PRODUCT_IMG ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    console.log(payload)

    let uploadTask = firebase.storage().ref().child('product').child(payload.doc).put(payload.img);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    function(snapshot) {
      commit('CAT_IMG_PROGRESS', snapshot, {root: true})
    }, function(error) {
      if (error.code) {
        console.log(error)
      }
    }, function() {
      commit('notify/NOTIFY', {title: 'Изображение товара загружено', type: 'success' },{root: true})
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        //Добавляем ссылку на аватарку в базу users
        firebase.firestore().collection('products').doc(payload.doc).set({
          img: downloadURL
        }, { merge: true })
        commit('CREATE_PRODUCT_IMG', downloadURL)
        commit('SET_PROCESSING', false, {root: true})
        
      })
    })
  },
  async GET_CATEGORIES ({ commit }, payload) {
    await db.collection('category').get()
    .then((doc) => {
      let categories = []
      doc.forEach(s => {
        const data = s.data()
        let category = {
          name: data.name,
          doc: data.doc
        }
        categories.push(category)
      })
      commit('GET_CATEGORIES', categories)
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  },
  SET_CATEGORIES ({ commit, getters }, payload) {
    // Добавляем выбранные категории в товар, а именно в коллекцию products
    db.collection('products').doc(payload.slug).update({
      categories: payload.checkedCategories
    })
    .then(() => {
      commit('notify/NOTIFY', {title: 'Категории добавлены в товар', type: 'success' }, {root: true})
    })
    .catch( error => {
      commit('notify/NOTIFY', {title: 'Категории не добавлены в товар', text: error, type: 'error' }, {root: true})
    })

    // Добавляем созданный товар в выбранные категории
    // payload.img = getters.isImg

    // payload.checkedCategories.forEach(doc => {
    //   commit('SET_CATEGORIES', this.getters.isDoc)
    //   db.collection('category').doc(doc.doc).collection('products').doc(payload.doc).set(payload)
    //   .then(() => {
    //     commit('notify/NOTIFY', {title: 'Товар разнесён по категориям', type: 'success' }, {root: true})
    //   })
    //   .catch( error => {
    //     commit('notify/NOTIFY', {title: 'Товар не разнесён по категориям', text: error, type: 'error' }, {root: true})
    //   })
    // })
  }
}