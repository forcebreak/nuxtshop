import * as firebase from 'firebase'
import firebaseConf from '../../plugins/firebase'
import 'firebase/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  doc: '',
  imgProgress: 0
})

export const getters = {
  isDoc (state) {
    return state.doc
  },
  imgProgress (state) {
    return state.avatarProgress
  }
}

export const mutations = {
  CREATE_CATEGORY (state, payload) {
    state.doc = payload
  },
  CAT_IMG_PROGRESS (state, payload) {
    let progress = (payload.bytesTransferred / payload.totalBytes) * 100;
    state.avatarProgress = progress
    switch (payload.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  },
}
export const actions = { 
  CREATE_CATEGORY ({ commit }, payload){
    db.collection('category').doc(payload.slug).set(payload)
    .then(() => {
      commit('CREATE_CATEGORY', payload.slug)
      commit('notify/NOTIFY', {title: 'Создана категория', text: payload.name, type: 'success' },{root: true})
    })
  },
  SET_CAT_IMG ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    // Upload file and metadata to the object 'images/mountains.jpg'
    let uploadTask = firebase.storage().ref().child('category/' + payload.doc).put(payload.img);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function(snapshot) {
      commit('CAT_IMG_PROGRESS', snapshot, {root: true})
    }, function(error) {

    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/unauthorized':
        // User doesn't have permission to access the object
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/canceled':
        // User canceled the upload
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/unknown':
        // Unknown error occurred, inspect error.serverResponse
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;
    }
    }, function() {
      // Upload completed successfully, now we can get the download URL
      commit('notify/NOTIFY', {title: 'Изображение категории загружено', type: 'success' },{root: true})
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        //Добавляем ссылку на аватарку в базу users
        firebase.firestore().collection('category').doc(payload.doc).set({
          img: downloadURL
        }, { merge: true })

        commit('SET_PROCESSING', false, {root: true})
        
      })
    })
  }
}