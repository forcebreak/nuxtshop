import * as firebase from 'firebase'

export const state = () => ({
  processing: false,
  cookieId: '',
  darkMode: true
})

export const getters = {
  processing (state) {
    return state.processing
  },
  darkMode(state) {
    return state.darkMode
  }
}

export const mutations = {
  SET_PROCESSING (state, payload) {
    state.processing = payload
  },
  SET_COOKIE (state, payload) {
    payload.path = payload.path || '/'; // заполняем путь если не заполнен
    payload.days = payload.days || 10;  // заполняем время жизни если не получен параметр

    var last_date = new Date();
    last_date.setDate(last_date.getDate() + payload.days);
    var value = escape(payload.value) + ((payload.days==null) ? "" : "; expires="+last_date.toUTCString());
    document.cookie = payload.name + "=" + value + "; path=" + payload.path; // вешаем куки
  },
  FILTER_COOKIE (state, payload) {
    let coockieArray = payload.headers.cookie.split(';')
    coockieArray.forEach((element,i) => {
      let newElement = element.split('=')
      if (newElement[0] == 'userId' || newElement[0] == ' userId'){
        state.cookieId = newElement[1]
      }  
    })
  },
  SET_APP_MODE(state, payload) {
    state.darkMode = payload
  }
}
export const actions = {
  // async nuxtServerInit ({commit}, {req, route}) {
  //   if (req.headers.cookie) {
  //     commit('FILTER_COOKIE', req)
  //     await firebase.firestore().collection('users').doc(this.state.cookieId).get()
  //     .then((doc) => {
  //       let result = doc.data()
  //       if (result != undefined){
  //         commit('user/SET_USER', result)
  //       }else{
  //         commit('user/UNSET_USER', doc.data())
  //       }
  //     })
  //   }
  // }
}