import * as firebase from 'firebase'
export const actions = {
  upload({commit}, payload) {
      firebase.firestore().collection('category').doc(payload.name).update({
        products: payload.products
      })
      .then(() => {
        commit('notify/NOTIFY', {title: 'Загружено', type: 'success' },{root: true})
      })
  },
  uploadCats({commit}, payload) {
    firebase.firestore().collection('category').doc(payload.name).set(payload)
    .then(() => {
      commit('notify/NOTIFY', {title: 'Загружено', type: 'success' },{root: true})
    })
  }
}